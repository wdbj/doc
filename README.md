---
home: true
layout: Blog
icon: home
title: 博客主页
heroImage: /logo.svg
heroText: 我的笔记
heroFullScreen: true
tagline: 随手记 方便你我他
projects:
  - icon: project
    name: 项目名称
    desc: 项目详细描述
    link: https://你的项目链接

  - icon: link
    name: 链接名称
    desc: 链接详细描述
    link: https://链接地址

  - icon: book
    name: 书籍名称
    desc: 书籍详细描述
    link: https://你的书籍链接

  - icon: article
    name: 文章名称
    desc: 文章详细描述
    link: https://你的文章链接

  - icon: friend
    name: 伙伴名称
    desc: 伙伴详细介绍
    link: https://你的伙伴链接

  - icon: /logo.svg
    name: 自定义项目
    desc: 自定义详细介绍
    link: https://你的自定义链接

# footer: 547176052@qq.com
---


<!-- 

## 文档地址
https://doc.142536.vip

## 简介
欢迎大家提交文档 随手一个笔记 方便你我他

## 提交方法

我也不不太清楚 应该是

克隆----创建分支----推送到分支----审核----合并发布 

-->