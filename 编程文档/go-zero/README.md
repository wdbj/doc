---
# 这是侧边栏索引
order: 1
# 这是页面的图标
icon: page
# 这是文章的标题
title: 开发环境搭建
# 设置作者
author: 野生程序员
# 设置写作时间
date: 2022-06-13
# 一个页面可以有多个分类
category:
  - 使用指南
# 一个页面可以有多个标签
tag:
  - 开发环境搭建
  - 使用指南
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在首页的文章板块中
star: true
# 你可以自定义页脚
# footer: qq547176052
comment: true
---

<!-- `more` 注释之前的内容被视为文章摘要。 -->

<!-- more -->
## Go 安装
Go安装前面已经做了介绍，请参考：Go-在windows11系统安装配置

## Go 配置

### go module 
go管理依赖工具
需要开启
命令行输入：
```
go env -w GO111MODULE="on"
```

### 修改go代理地址
```
go env -w GOPROXY=https://goproxy.cn
```


### 查看env文件可以使用
```
go env
```


### Goctl 安装
goctl 是 go-zero 微服务框架下的代码生成工具

安装命令：
```
go install github.com/zeromicro/go-zero/tools/goctl@latest
```


> 注意：由于版本问题，go get 需要改成 go install

通过此命令，可以将 goctl 工具安装到 `%GOPATH%/bin` 目录下。

`%GOPATH%` 如何配置，请参考： 

Go-在windows11系统安装配置

https://blog.csdn.net/mokyzone/article/details/124323805?spm=1001.2014.3001.5502

验证是否成功，执行
```
goctl -v
```

显示版本，即代表安装成功

## Protoc 安装

打开 protobuf

https://github.com/protocolbuffers/protobuf/releases

选择适合自己win系统的压缩包文件，下载压缩包

解压文件，把解压
找到目录文件：bin\protoc.exe

把
protoc.exe
拷贝到 
%GOPATH%/bin
目录即可
检查是否安装好，输入：
```
protoc --version
```
![](./img/2022_06_13_23_11_55.png)

至此，windows11下配置go-zero环境已经完成。

## 测试项目
#### 下载
https://github.com/zeromicro/go-zero-demo
#### 在 go-zero-demo 目录下
```
go mod tidy
```
#### 启动etcd
下载

https://github.com/etcd-io/etcd/releases

双击etcd.exe运行etcd服务

#### 启动user rpc
在 mall/user/rpc 目录

```
go run user.go -f etc/user.yaml
```
Starting rpc server at 127.0.0.1:8080...

#### 启动order api

在 mall/order/api 目录
```
go run order.go -f etc/order.yaml
```
Starting server at 0.0.0.0:8888...


#### 访问order api

http://localhost:8888/api/order/get/1


![](./img/2022_06_13_23_53_11.png)


## vscode高亮插件
```
goctl
```


## 官方文档

https://legacy.go-zero.dev/cn/concept-introduction.html