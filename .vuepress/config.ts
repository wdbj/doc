import { defineUserConfig } from "vuepress";
import theme from "./theme";
// import { hopeTheme } from "vuepress-theme-hope";
// import {PluginConfig} from "@vuepress/core/lib/types/plugin";
const {searchPlugin} = require('@vuepress/plugin-search')
export default defineUserConfig({
  lang: "zh-CN",
  title: "我的笔记",
  description: "随手记 方便你我他",
  base: "/",
  plugins: [
    searchPlugin({
      locales: {
        "/": {
          placeholder: "搜索",
        },
      },
    }),
  ],

  head: [
    // ['meta', { name: 'foo', content: 'bar' }],
    // ['link', { rel: 'canonical', href: 'foobar' }],
    ['script', {}, `
		//据说会提高百度收录
    	(function(){
			var bp = document.createElement('script');
			var curProtocol = window.location.protocol.split(':')[0];
			if (curProtocol === 'https'){
			bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
			}
			else{
			bp.src = 'http://push.zhanzhang.baidu.com/push.js';
			}
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(bp, s);
		})();

    	console.log('你好啊');
    `],
  ],
  // dest: './dist/docs',
  // dest: './public',
  theme,
});
