import {
  isPerformanceSupported,
  now,
  setupDevtoolsPlugin
} from "./chunk-H2L2JEI5.js";
import "./chunk-F6TR6WRY.js";
export {
  isPerformanceSupported,
  now,
  setupDevtoolsPlugin
};
//# sourceMappingURL=@vue_devtools-api.js.map
