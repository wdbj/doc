export const data = JSON.parse("{\"key\":\"v-23e0cde1\",\"path\":\"/%E7%BC%96%E7%A8%8B%E6%96%87%E6%A1%A3/go-zero/\",\"title\":\"开发环境搭建\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":1,\"icon\":\"page\",\"title\":\"开发环境搭建\",\"author\":\"野生程序员\",\"date\":\"2022-06-13T00:00:00.000Z\",\"category\":[\"使用指南\"],\"tag\":[\"开发环境搭建\",\"使用指南\"],\"sticky\":true,\"star\":true,\"comment\":true,\"summary\":\"\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E7%BC%96%E7%A8%8B%E6%96%87%E6%A1%A3/go-zero/\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"开发环境搭建\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:image\",\"content\":\"https://doc.142536.vip/\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"name\":\"twitter:card\",\"content\":\"summary_large_image\"}],[\"meta\",{\"name\":\"twitter:image:alt\",\"content\":\"开发环境搭建\"}],[\"meta\",{\"property\":\"article:author\",\"content\":\"野生程序员\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"开发环境搭建\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"使用指南\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2022-06-13T00:00:00.000Z\"}]]},\"excerpt\":\"<!-- `more` 注释之前的内容被视为文章摘要。 -->\\n\",\"headers\":[{\"level\":2,\"title\":\"Go 安装\",\"slug\":\"go-安装\",\"children\":[]},{\"level\":2,\"title\":\"Go 配置\",\"slug\":\"go-配置\",\"children\":[{\"level\":3,\"title\":\"go module\",\"slug\":\"go-module\",\"children\":[]},{\"level\":3,\"title\":\"修改go代理地址\",\"slug\":\"修改go代理地址\",\"children\":[]},{\"level\":3,\"title\":\"查看env文件可以使用\",\"slug\":\"查看env文件可以使用\",\"children\":[]},{\"level\":3,\"title\":\"Goctl 安装\",\"slug\":\"goctl-安装\",\"children\":[]}]},{\"level\":2,\"title\":\"Protoc 安装\",\"slug\":\"protoc-安装\",\"children\":[]},{\"level\":2,\"title\":\"测试项目\",\"slug\":\"测试项目\",\"children\":[]},{\"level\":2,\"title\":\"vscode高亮插件\",\"slug\":\"vscode高亮插件\",\"children\":[]},{\"level\":2,\"title\":\"官方文档\",\"slug\":\"官方文档\",\"children\":[]}],\"readingTime\":{\"minutes\":1.35,\"words\":405},\"filePathRelative\":\"编程文档/go-zero/README.md\",\"localizedDate\":\"2022年6月13日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
