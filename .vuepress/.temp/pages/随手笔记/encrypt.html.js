export const data = JSON.parse("{\"key\":\"v-281650f8\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/encrypt.html\",\"title\":\"密码加密的文章\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":4,\"icon\":\"lock\",\"category\":[\"使用指南\"],\"date\":\"2022-06-06T00:00:00.000Z\",\"tag\":[\"文章加密\"],\"summary\":\"密码加密的文章 实际的文章内容。 段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字段落 1 文字。 段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字段落 2 文字\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/encrypt.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"密码加密的文章\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"文章加密\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2022-06-06T00:00:00.000Z\"}]]},\"excerpt\":\"\",\"headers\":[],\"readingTime\":{\"minutes\":0.48,\"words\":144},\"filePathRelative\":\"随手笔记/encrypt.md\",\"localizedDate\":\"2022年6月6日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
