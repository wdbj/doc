export const data = JSON.parse("{\"key\":\"v-e7e1c6be\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E9%A1%B5%E9%9D%A2%E9%85%8D%E7%BD%AE.html\",\"title\":\"页面配置\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":2,\"icon\":\"page\",\"title\":\"页面配置\",\"author\":\"野生程序员\",\"date\":\"2022-06-06T00:00:00.000Z\",\"category\":[\"使用指南\"],\"tag\":[\"页面配置\",\"使用指南\"],\"sticky\":true,\"star\":true,\"comment\":true,\"summary\":\"more 注释之前的内容被视为文章摘要。\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E9%A1%B5%E9%9D%A2%E9%85%8D%E7%BD%AE.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"页面配置\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:author\",\"content\":\"野生程序员\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"页面配置\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"使用指南\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2022-06-06T00:00:00.000Z\"}]]},\"excerpt\":\"<p><code v-pre>more</code> 注释之前的内容被视为文章摘要。</p>\\n\",\"headers\":[{\"level\":2,\"title\":\"配置示例\",\"slug\":\"配置示例\",\"children\":[]},{\"level\":2,\"title\":\"页面信息\",\"slug\":\"页面信息\",\"children\":[]},{\"level\":2,\"title\":\"页面内容\",\"slug\":\"页面内容\",\"children\":[]},{\"level\":2,\"title\":\"页面结构\",\"slug\":\"页面结构\",\"children\":[]}],\"readingTime\":{\"minutes\":1.14,\"words\":343},\"filePathRelative\":\"随手笔记/页面配置.md\",\"localizedDate\":\"2022年6月6日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
