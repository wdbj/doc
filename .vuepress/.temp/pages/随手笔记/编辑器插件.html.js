export const data = JSON.parse("{\"key\":\"v-ee1cccb8\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E7%BC%96%E8%BE%91%E5%99%A8%E6%8F%92%E4%BB%B6.html\",\"title\":\"编辑器插件\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":4,\"icon\":\"page\",\"title\":\"编辑器插件\",\"author\":\"野生程序员\",\"date\":\"2022-06-06T00:00:00.000Z\",\"category\":[\"vscode插件\"],\"tag\":[\"vscode插件\",\"使用指南\"],\"sticky\":true,\"star\":true,\"comment\":true,\"summary\":\"more 唯一缺点 粘贴快捷键不是 ctrl + v\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E7%BC%96%E8%BE%91%E5%99%A8%E6%8F%92%E4%BB%B6.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"编辑器插件\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:author\",\"content\":\"野生程序员\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"vscode插件\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"使用指南\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2022-06-06T00:00:00.000Z\"}]]},\"excerpt\":\"<p><code v-pre>more</code> 唯一缺点 粘贴快捷键不是 ctrl + v</p>\\n\",\"headers\":[{\"level\":2,\"title\":\"编辑器使用 vscode-paste-image 这个插件\",\"slug\":\"编辑器使用-vscode-paste-image-这个插件\",\"children\":[]}],\"readingTime\":{\"minutes\":0.23,\"words\":69},\"filePathRelative\":\"随手笔记/编辑器插件.md\",\"localizedDate\":\"2022年6月6日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
