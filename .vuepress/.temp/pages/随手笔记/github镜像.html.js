export const data = JSON.parse("{\"key\":\"v-a929ce5e\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/github%E9%95%9C%E5%83%8F.html\",\"title\":\"github镜像\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":10,\"icon\":\"page\",\"title\":\"github镜像\",\"author\":\"野生程序员\",\"date\":\"2022-06-14T00:00:00.000Z\",\"category\":[\"vscode插件\"],\"tag\":[\"github镜像\",\"github\"],\"sticky\":true,\"star\":true,\"comment\":true,\"summary\":\"more 唯一缺点 粘贴快捷键不是 ctrl + v\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/github%E9%95%9C%E5%83%8F.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"github镜像\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:author\",\"content\":\"野生程序员\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"github镜像\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"github\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2022-06-14T00:00:00.000Z\"}]]},\"excerpt\":\"<p><code v-pre>more</code> 唯一缺点 粘贴快捷键不是 ctrl + v</p>\\n\",\"headers\":[{\"level\":2,\"title\":\"镜像网站\",\"slug\":\"镜像网站\",\"children\":[]}],\"readingTime\":{\"minutes\":0.07,\"words\":21},\"filePathRelative\":\"随手笔记/github镜像.md\",\"localizedDate\":\"2022年6月14日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
