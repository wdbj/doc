export const data = JSON.parse("{\"key\":\"v-4fb4fe2c\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/slide.html\",\"title\":\"幻灯片页\",\"lang\":\"zh-CN\",\"frontmatter\":{\"title\":\"幻灯片页\",\"icon\":\"slides\",\"layout\":\"Slide\",\"summary\":\"@slidestart 幻灯片演示 一个简单的幻灯片演示与各种小贴士。\\r\\\" 作者 Mr.Hope. 请滚动鼠标滚轮进入下一页\\\" --- 标注幻灯片 👇 -- 标注幻灯片 使用 --- 标注水平幻灯片 在水平幻灯片中使用 -- 分割垂直幻灯片 使用 `` 在幻灯片上添加属性 使用 `` 在前一个 HTML 元素上添加属性 --- Markdown 你可以在幻\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/slide.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"幻灯片页\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}]]},\"excerpt\":\"\",\"headers\":[],\"readingTime\":{\"minutes\":4.47,\"words\":1342},\"filePathRelative\":\"随手笔记/slide.md\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
