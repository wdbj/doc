export const data = JSON.parse("{\"key\":\"v-b72b7870\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E7%BB%84%E4%BB%B6%E7%A6%81%E7%94%A8.html\",\"title\":\"组件禁用\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":3,\"title\":\"组件禁用\",\"icon\":\"config\",\"category\":[\"使用指南\"],\"tag\":[\"禁用\"],\"navbar\":false,\"sidebar\":false,\"breadcrumb\":false,\"pageInfo\":false,\"contributors\":false,\"editLink\":false,\"lastUpdated\":false,\"prev\":false,\"next\":false,\"comment\":true,\"footer\":false,\"backtotop\":false,\"summary\":\"你可以通过设置页面的 Frontmatter，在页面禁用一些功能。\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E7%BB%84%E4%BB%B6%E7%A6%81%E7%94%A8.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"组件禁用\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"禁用\"}]]},\"excerpt\":\"<p>你可以通过设置页面的 Frontmatter，在页面禁用一些功能。</p>\\n\",\"headers\":[],\"readingTime\":{\"minutes\":0.25,\"words\":74},\"filePathRelative\":\"随手笔记/组件禁用.md\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
