export const data = JSON.parse("{\"key\":\"v-6651b932\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/vercel%E8%87%AA%E5%8A%A8%E6%9E%84%E5%BB%BA%E5%89%8D%E7%AB%AF%E7%BD%91%E7%AB%99.html\",\"title\":\"vercel部署前端静态项目\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":3,\"icon\":\"page\",\"title\":\"vercel部署前端静态项目\",\"author\":\"野生程序员\",\"date\":\"2022-06-06T00:00:00.000Z\",\"category\":[\"使用指南\"],\"tag\":[\"vercel\",\"使用指南\"],\"sticky\":true,\"star\":true,\"footer\":\"qq547176052\",\"comment\":true,\"summary\":\"more 注释之前的内容被视为文章摘要。\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/vercel%E8%87%AA%E5%8A%A8%E6%9E%84%E5%BB%BA%E5%89%8D%E7%AB%AF%E7%BD%91%E7%AB%99.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"vercel部署前端静态项目\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:author\",\"content\":\"野生程序员\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"vercel\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"使用指南\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2022-06-06T00:00:00.000Z\"}]]},\"excerpt\":\"<p><code v-pre>more</code> 注释之前的内容被视为文章摘要。</p>\\n\",\"headers\":[{\"level\":2,\"title\":\"地址\",\"slug\":\"地址\",\"children\":[]},{\"level\":2,\"title\":\"gitee 触发自动构建\",\"slug\":\"gitee-触发自动构建\",\"children\":[]}],\"readingTime\":{\"minutes\":0.11,\"words\":32},\"filePathRelative\":\"随手笔记/vercel自动构建前端网站.md\",\"localizedDate\":\"2022年6月6日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
