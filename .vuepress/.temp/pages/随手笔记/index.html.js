export const data = JSON.parse("{\"key\":\"v-8edafd38\",\"path\":\"/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/\",\"title\":\"\",\"lang\":\"zh-CN\",\"frontmatter\":{\"index\":false,\"icon\":\"creative\",\"category\":[\"使用指南\"],\"summary\":\"guide首页 页面展示; Markdown 展示; 禁用展示; 加密展示;\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}]]},\"excerpt\":\"\",\"headers\":[{\"level\":2,\"title\":\"guide首页\",\"slug\":\"guide首页\",\"children\":[]}],\"readingTime\":{\"minutes\":0.07,\"words\":22},\"filePathRelative\":\"随手笔记/README.md\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
