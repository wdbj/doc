export const data = JSON.parse("{\"key\":\"v-76a05d8a\",\"path\":\"/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE1/git.html\",\"title\":\"git命令\",\"lang\":\"zh-CN\",\"frontmatter\":{\"order\":1,\"icon\":\"page\",\"title\":\"git命令\",\"author\":\"野生程序员\",\"date\":\"2020-06-06T00:00:00.000Z\",\"category\":[\"git\"],\"tag\":[\"git\"],\"sticky\":true,\"star\":true,\"summary\":\"more git命令\\n\",\"head\":[[\"meta\",{\"property\":\"og:url\",\"content\":\"https://doc.142536.vip/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE1/git.html\"}],[\"meta\",{\"property\":\"og:site_name\",\"content\":\"我的笔记\"}],[\"meta\",{\"property\":\"og:title\",\"content\":\"git命令\"}],[\"meta\",{\"property\":\"og:type\",\"content\":\"article\"}],[\"meta\",{\"property\":\"og:locale\",\"content\":\"zh-CN\"}],[\"meta\",{\"property\":\"article:author\",\"content\":\"野生程序员\"}],[\"meta\",{\"property\":\"article:tag\",\"content\":\"git\"}],[\"meta\",{\"property\":\"article:published_time\",\"content\":\"2020-06-06T00:00:00.000Z\"}]]},\"excerpt\":\"<p><code v-pre>more</code> git命令</p>\\n\",\"headers\":[{\"level\":2,\"title\":\"代理\",\"slug\":\"代理\",\"children\":[]},{\"level\":2,\"title\":\"取消代理\",\"slug\":\"取消代理\",\"children\":[]},{\"level\":2,\"title\":\"克隆项目\",\"slug\":\"克隆项目\",\"children\":[]},{\"level\":2,\"title\":\"在命令行上创建一个新的仓库\",\"slug\":\"在命令行上创建一个新的仓库\",\"children\":[]},{\"level\":2,\"title\":\"推送现有的仓库\",\"slug\":\"推送现有的仓库\",\"children\":[]},{\"level\":2,\"title\":\"强制推送\",\"slug\":\"强制推送\",\"children\":[]},{\"level\":2,\"title\":\"查看当前仓库地址\",\"slug\":\"查看当前仓库地址\",\"children\":[]},{\"level\":2,\"title\":\"设置新的仓库地址\",\"slug\":\"设置新的仓库地址\",\"children\":[]},{\"level\":2,\"title\":\"本地项目恢复到指定版本\",\"slug\":\"本地项目恢复到指定版本\",\"children\":[]},{\"level\":2,\"title\":\"分支\",\"slug\":\"分支\",\"children\":[{\"level\":3,\"title\":\"创建分支\",\"slug\":\"创建分支\",\"children\":[]},{\"level\":3,\"title\":\"切换到分支\",\"slug\":\"切换到分支\",\"children\":[]},{\"level\":3,\"title\":\"推送到分支\",\"slug\":\"推送到分支\",\"children\":[]}]},{\"level\":2,\"title\":\"设置\",\"slug\":\"设置\",\"children\":[]}],\"readingTime\":{\"minutes\":1.04,\"words\":313},\"filePathRelative\":\"项目文档/项目1/git.md\",\"localizedDate\":\"2020年6月6日\"}")

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updatePageData) {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ data }) => {
    __VUE_HMR_RUNTIME__.updatePageData(data)
  })
}
