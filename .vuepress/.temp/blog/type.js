export const typeMap = {"article":{"/":{"path":"/article/","keys":["v-a929ce5e","v-23e0cde1","v-6651b932","v-ee1cccb8","v-e7e1c6be","v-9820bcaa","v-76a05d8a","v-281650f8","v-f0bf1960","v-5bf6ae7c","v-421613d1","v-5dd0019b","v-6d5ee266","v-8edafd38","v-4fb4fe2c","v-b72b7870"]}},"encrypted":{"/":{"path":"/encrypted/","keys":[]}},"slide":{"/":{"path":"/slide/","keys":["v-4fb4fe2c"]}},"star":{"/":{"path":"/star/","keys":["v-a929ce5e","v-23e0cde1","v-6651b932","v-ee1cccb8","v-e7e1c6be","v-9820bcaa","v-76a05d8a"]}},"timeline":{"/":{"path":"/timeline/","keys":["v-a929ce5e","v-23e0cde1","v-281650f8","v-6651b932","v-ee1cccb8","v-e7e1c6be","v-f0bf1960","v-5bf6ae7c","v-421613d1","v-5dd0019b","v-9820bcaa","v-76a05d8a"]}}}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updateBlogType) {
    __VUE_HMR_RUNTIME__.updateBlogType(typeMap)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ typeMap }) => {
    __VUE_HMR_RUNTIME__.updateBlogType(typeMap)
  })
}
