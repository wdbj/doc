export const categoryMap = {"category":{"/":{"path":"/category/","map":{"使用指南":{"path":"/category/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/","keys":["v-23e0cde1","v-6651b932","v-e7e1c6be","v-281650f8","v-6d5ee266","v-8edafd38","v-b72b7870"]},"git":{"path":"/category/git/","keys":["v-9820bcaa","v-76a05d8a"]},"vscode插件":{"path":"/category/vscode%E6%8F%92%E4%BB%B6/","keys":["v-a929ce5e","v-ee1cccb8"]},"CategoryA":{"path":"/category/categorya/","keys":["v-f0bf1960","v-5bf6ae7c","v-421613d1","v-5dd0019b"]},"CategoryB":{"path":"/category/categoryb/","keys":["v-f0bf1960","v-5bf6ae7c","v-421613d1","v-5dd0019b"]}}}},"tag":{"/":{"path":"/tag/","map":{"文章加密":{"path":"/tag/%E6%96%87%E7%AB%A0%E5%8A%A0%E5%AF%86/","keys":["v-281650f8"]},"git":{"path":"/tag/git/","keys":["v-9820bcaa","v-76a05d8a"]},"github镜像":{"path":"/tag/github%E9%95%9C%E5%83%8F/","keys":["v-a929ce5e"]},"github":{"path":"/tag/github/","keys":["v-a929ce5e"]},"Markdown":{"path":"/tag/markdown/","keys":["v-6d5ee266"]},"vercel":{"path":"/tag/vercel/","keys":["v-6651b932"]},"使用指南":{"path":"/tag/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/","keys":["v-23e0cde1","v-6651b932","v-ee1cccb8","v-e7e1c6be"]},"禁用":{"path":"/tag/%E7%A6%81%E7%94%A8/","keys":["v-b72b7870"]},"vscode插件":{"path":"/tag/vscode%E6%8F%92%E4%BB%B6/","keys":["v-ee1cccb8"]},"页面配置":{"path":"/tag/%E9%A1%B5%E9%9D%A2%E9%85%8D%E7%BD%AE/","keys":["v-e7e1c6be"]},"tag A":{"path":"/tag/tag-a/","keys":["v-f0bf1960","v-5bf6ae7c","v-421613d1","v-5dd0019b"]},"tag B":{"path":"/tag/tag-b/","keys":["v-f0bf1960","v-5bf6ae7c","v-421613d1","v-5dd0019b"]},"开发环境搭建":{"path":"/tag/%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/","keys":["v-23e0cde1"]}}}}}

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updateBlogCategory) {
    __VUE_HMR_RUNTIME__.updateBlogCategory(categoryMap)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ categoryMap }) => {
    __VUE_HMR_RUNTIME__.updateBlogCategory(categoryMap)
  })
}
