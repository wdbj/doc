export const searchIndex = [
  {
    "title": "博客主页",
    "headers": [],
    "path": "/home.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "博客主页",
    "headers": [],
    "path": "/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "密码加密的文章",
    "headers": [],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/encrypt.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "git命令",
    "headers": [
      {
        "level": 2,
        "title": "代理",
        "slug": "代理",
        "children": []
      },
      {
        "level": 2,
        "title": "取消代理",
        "slug": "取消代理",
        "children": []
      },
      {
        "level": 2,
        "title": "克隆项目",
        "slug": "克隆项目",
        "children": []
      },
      {
        "level": 2,
        "title": "在命令行上创建一个新的仓库",
        "slug": "在命令行上创建一个新的仓库",
        "children": []
      },
      {
        "level": 2,
        "title": "推送现有的仓库",
        "slug": "推送现有的仓库",
        "children": []
      },
      {
        "level": 2,
        "title": "强制推送",
        "slug": "强制推送",
        "children": []
      },
      {
        "level": 2,
        "title": "查看当前仓库地址",
        "slug": "查看当前仓库地址",
        "children": []
      },
      {
        "level": 2,
        "title": "设置新的仓库地址",
        "slug": "设置新的仓库地址",
        "children": []
      },
      {
        "level": 2,
        "title": "本地项目恢复到指定版本",
        "slug": "本地项目恢复到指定版本",
        "children": []
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/git.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "github镜像",
    "headers": [
      {
        "level": 2,
        "title": "镜像网站",
        "slug": "镜像网站",
        "children": []
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/github%E9%95%9C%E5%83%8F.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "Markdown 增强",
    "headers": [
      {
        "level": 2,
        "title": "Markdown 介绍",
        "slug": "markdown-介绍",
        "children": []
      },
      {
        "level": 2,
        "title": "VuePress 扩展",
        "slug": "vuepress-扩展",
        "children": []
      },
      {
        "level": 2,
        "title": "主题扩展",
        "slug": "主题扩展",
        "children": [
          {
            "level": 3,
            "title": "自定义容器",
            "slug": "自定义容器",
            "children": []
          },
          {
            "level": 3,
            "title": "代码块",
            "slug": "代码块",
            "children": []
          },
          {
            "level": 3,
            "title": "自定义对齐",
            "slug": "自定义对齐",
            "children": []
          },
          {
            "level": 3,
            "title": "上下角标",
            "slug": "上下角标",
            "children": []
          },
          {
            "level": 3,
            "title": "脚注",
            "slug": "脚注",
            "children": []
          },
          {
            "level": 3,
            "title": "标记",
            "slug": "标记",
            "children": []
          },
          {
            "level": 3,
            "title": "任务列表",
            "slug": "任务列表",
            "children": []
          },
          {
            "level": 3,
            "title": "图表",
            "slug": "图表",
            "children": []
          },
          {
            "level": 3,
            "title": "流程图",
            "slug": "流程图",
            "children": []
          },
          {
            "level": 3,
            "title": "Mermaid",
            "slug": "mermaid",
            "children": []
          },
          {
            "level": 3,
            "title": "Tex 语法",
            "slug": "tex-语法",
            "children": []
          },
          {
            "level": 3,
            "title": "代码演示",
            "slug": "代码演示",
            "children": []
          },
          {
            "level": 3,
            "title": "幻灯片",
            "slug": "幻灯片",
            "children": []
          }
        ]
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/markdown.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "",
    "headers": [
      {
        "level": 2,
        "title": "guide首页",
        "slug": "guide首页",
        "children": []
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "幻灯片页",
    "headers": [],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/slide.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "vercel部署前端静态项目",
    "headers": [
      {
        "level": 2,
        "title": "地址",
        "slug": "地址",
        "children": []
      },
      {
        "level": 2,
        "title": "gitee 触发自动构建",
        "slug": "gitee-触发自动构建",
        "children": []
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/vercel%E8%87%AA%E5%8A%A8%E6%9E%84%E5%BB%BA%E5%89%8D%E7%AB%AF%E7%BD%91%E7%AB%99.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "组件禁用",
    "headers": [],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E7%BB%84%E4%BB%B6%E7%A6%81%E7%94%A8.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "编辑器插件",
    "headers": [
      {
        "level": 2,
        "title": "编辑器使用 vscode-paste-image 这个插件",
        "slug": "编辑器使用-vscode-paste-image-这个插件",
        "children": []
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E7%BC%96%E8%BE%91%E5%99%A8%E6%8F%92%E4%BB%B6.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "页面配置",
    "headers": [
      {
        "level": 2,
        "title": "配置示例",
        "slug": "配置示例",
        "children": []
      },
      {
        "level": 2,
        "title": "页面信息",
        "slug": "页面信息",
        "children": []
      },
      {
        "level": 2,
        "title": "页面内容",
        "slug": "页面内容",
        "children": []
      },
      {
        "level": 2,
        "title": "页面结构",
        "slug": "页面结构",
        "children": []
      }
    ],
    "path": "/%E9%9A%8F%E6%89%8B%E7%AC%94%E8%AE%B0/%E9%A1%B5%E9%9D%A2%E9%85%8D%E7%BD%AE.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "git命令",
    "headers": [
      {
        "level": 2,
        "title": "代理",
        "slug": "代理",
        "children": []
      },
      {
        "level": 2,
        "title": "取消代理",
        "slug": "取消代理",
        "children": []
      },
      {
        "level": 2,
        "title": "克隆项目",
        "slug": "克隆项目",
        "children": []
      },
      {
        "level": 2,
        "title": "在命令行上创建一个新的仓库",
        "slug": "在命令行上创建一个新的仓库",
        "children": []
      },
      {
        "level": 2,
        "title": "推送现有的仓库",
        "slug": "推送现有的仓库",
        "children": []
      },
      {
        "level": 2,
        "title": "强制推送",
        "slug": "强制推送",
        "children": []
      },
      {
        "level": 2,
        "title": "查看当前仓库地址",
        "slug": "查看当前仓库地址",
        "children": []
      },
      {
        "level": 2,
        "title": "设置新的仓库地址",
        "slug": "设置新的仓库地址",
        "children": []
      },
      {
        "level": 2,
        "title": "本地项目恢复到指定版本",
        "slug": "本地项目恢复到指定版本",
        "children": []
      },
      {
        "level": 2,
        "title": "分支",
        "slug": "分支",
        "children": [
          {
            "level": 3,
            "title": "创建分支",
            "slug": "创建分支",
            "children": []
          },
          {
            "level": 3,
            "title": "切换到分支",
            "slug": "切换到分支",
            "children": []
          },
          {
            "level": 3,
            "title": "推送到分支",
            "slug": "推送到分支",
            "children": []
          }
        ]
      },
      {
        "level": 2,
        "title": "设置",
        "slug": "设置",
        "children": []
      }
    ],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE1/git.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "项目主页",
    "headers": [],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE1/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "开发环境搭建",
    "headers": [
      {
        "level": 2,
        "title": "标题 2",
        "slug": "标题-2",
        "children": [
          {
            "level": 3,
            "title": "标题 3",
            "slug": "标题-3",
            "children": []
          }
        ]
      }
    ],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE1/%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "文档1",
    "headers": [
      {
        "level": 2,
        "title": "标题 2",
        "slug": "标题-2",
        "children": [
          {
            "level": 3,
            "title": "标题 3",
            "slug": "标题-3",
            "children": []
          }
        ]
      }
    ],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE1/%E6%96%87%E6%A1%A31.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "项目主页",
    "headers": [],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE2/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "开发环境搭建2",
    "headers": [
      {
        "level": 2,
        "title": "标题 2",
        "slug": "标题-2",
        "children": [
          {
            "level": 3,
            "title": "标题 3",
            "slug": "标题-3",
            "children": []
          }
        ]
      }
    ],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE2/%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "文档1",
    "headers": [
      {
        "level": 2,
        "title": "标题 2",
        "slug": "标题-2",
        "children": [
          {
            "level": 3,
            "title": "标题 3",
            "slug": "标题-3",
            "children": []
          }
        ]
      }
    ],
    "path": "/%E9%A1%B9%E7%9B%AE%E6%96%87%E6%A1%A3/%E9%A1%B9%E7%9B%AE2/%E6%96%87%E6%A1%A31.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "项目主页",
    "headers": [],
    "path": "/%E7%BC%96%E7%A8%8B%E6%96%87%E6%A1%A3/docker/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "开发环境搭建",
    "headers": [
      {
        "level": 2,
        "title": "Go 安装",
        "slug": "go-安装",
        "children": []
      },
      {
        "level": 2,
        "title": "Go 配置",
        "slug": "go-配置",
        "children": [
          {
            "level": 3,
            "title": "go module",
            "slug": "go-module",
            "children": []
          },
          {
            "level": 3,
            "title": "修改go代理地址",
            "slug": "修改go代理地址",
            "children": []
          },
          {
            "level": 3,
            "title": "查看env文件可以使用",
            "slug": "查看env文件可以使用",
            "children": []
          },
          {
            "level": 3,
            "title": "Goctl 安装",
            "slug": "goctl-安装",
            "children": []
          }
        ]
      },
      {
        "level": 2,
        "title": "Protoc 安装",
        "slug": "protoc-安装",
        "children": []
      },
      {
        "level": 2,
        "title": "测试项目",
        "slug": "测试项目",
        "children": []
      },
      {
        "level": 2,
        "title": "vscode高亮插件",
        "slug": "vscode高亮插件",
        "children": []
      },
      {
        "level": 2,
        "title": "官方文档",
        "slug": "官方文档",
        "children": []
      }
    ],
    "path": "/%E7%BC%96%E7%A8%8B%E6%96%87%E6%A1%A3/go-zero/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "",
    "headers": [],
    "path": "/404.html",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "分类",
    "headers": [],
    "path": "/category/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "标签",
    "headers": [],
    "path": "/tag/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "文章",
    "headers": [],
    "path": "/article/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "加密",
    "headers": [],
    "path": "/encrypted/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "幻灯片",
    "headers": [],
    "path": "/slide/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "收藏",
    "headers": [],
    "path": "/star/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "时间轴",
    "headers": [],
    "path": "/timeline/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "使用指南 分类",
    "headers": [],
    "path": "/category/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "文章加密 标签",
    "headers": [],
    "path": "/tag/%E6%96%87%E7%AB%A0%E5%8A%A0%E5%AF%86/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "git 分类",
    "headers": [],
    "path": "/category/git/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "git 标签",
    "headers": [],
    "path": "/tag/git/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "vscode插件 分类",
    "headers": [],
    "path": "/category/vscode%E6%8F%92%E4%BB%B6/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "github镜像 标签",
    "headers": [],
    "path": "/tag/github%E9%95%9C%E5%83%8F/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "CategoryA 分类",
    "headers": [],
    "path": "/category/categorya/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "github 标签",
    "headers": [],
    "path": "/tag/github/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "CategoryB 分类",
    "headers": [],
    "path": "/category/categoryb/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "Markdown 标签",
    "headers": [],
    "path": "/tag/markdown/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "vercel 标签",
    "headers": [],
    "path": "/tag/vercel/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "使用指南 标签",
    "headers": [],
    "path": "/tag/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "禁用 标签",
    "headers": [],
    "path": "/tag/%E7%A6%81%E7%94%A8/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "vscode插件 标签",
    "headers": [],
    "path": "/tag/vscode%E6%8F%92%E4%BB%B6/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "页面配置 标签",
    "headers": [],
    "path": "/tag/%E9%A1%B5%E9%9D%A2%E9%85%8D%E7%BD%AE/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "tag A 标签",
    "headers": [],
    "path": "/tag/tag-a/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "tag B 标签",
    "headers": [],
    "path": "/tag/tag-b/",
    "pathLocale": "/",
    "extraFields": []
  },
  {
    "title": "开发环境搭建 标签",
    "headers": [],
    "path": "/tag/%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/",
    "pathLocale": "/",
    "extraFields": []
  }
]

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updateSearchIndex) {
    __VUE_HMR_RUNTIME__.updateSearchIndex(searchIndex)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ searchIndex }) => {
    __VUE_HMR_RUNTIME__.updateSearchIndex(searchIndex)
  })
}
