import { hopeTheme } from "vuepress-theme-hope";
import navbar from "./navbar";
import sidebar from "./sidebar";



export default hopeTheme({

  encrypt: {
    //加密配置
    config: {
      "/doc/encrypt.html": ["1234"],
      // "/随手记/": ["1234"],
      // "/doc/jiami.html": ["1234"],
    },
  },

  //网站部署域名
  hostname: "https://doc.142536.vip",
  //全局默认作者
  author: {
    name: "野生程序员",
    url: "https://doc.142536.vip",
  },
  //不能用
  // home: "/home.html",


  // navbarLayout: {
  //   left: ["Brand"],
  //   center: ["Links"],
  //   right: ["Language", "Repo", "Outlook", "Search"],
  // },



  //字体图标资源链接
  iconAssets: "//at.alicdn.com/t/font_2410206_a0xb9hku9iu.css",
  logo: "/logo.svg",
  //顶部导航右边仓库
  // repo: "qq547176052/qq547176052.github.io",
  //文档在仓库中的目录
  docsDir: "demo/src",

  // 导航栏配置
  navbar: navbar,
  // 侧边栏配置
  sidebar: sidebar,
  //默认页脚
  footer: "邮箱:547176052@qq.com",
  displayFooter: true,
  //文章信息配置
  pageInfo: ["Author", "Original", "Date", "Category", "Tag", "ReadingTime"],

  blog: {
    description: "随手记 方便你我他",
    intro: "/intro.html",
    medias: {
      // Baidu: "https://example.com",
      // Bitbucket: "https://example.com",
      // Dingding: "https://example.com",
      // Discord: "https://example.com",
      // Dribbble: "https://example.com",
      // Email: "https://example.com",
      // Evernote: "https://example.com",
      // Facebook: "https://example.com",
      // Flipboard: "https://example.com",
      // Gitee: "https://example.com",
      // GitHub: "https://example.com",
      // Gitlab: "https://example.com",
      // Gmail: "https://example.com",
      // Instagram: "https://example.com",
      // Lines: "https://example.com",
      // Linkedin: "https://example.com",
      // Pinterest: "https://example.com",
      // Pocket: "https://example.com",
      QQ: "http://wpa.qq.com/msgrd?v=3&uin=547176052&site=qq&menu=yes",
      // Qzone: "https://example.com",
      // Reddit: "https://example.com",
      // Rss: "https://example.com",
      // Steam: "https://example.com",
      // Twitter: "https://example.com",
      // Wechat: "https://example.com",
      // Weibo: "https://example.com",
      // Whatsapp: "https://example.com",
      // Youtube: "https://example.com",
      // Zhihu: "https://example.com",
    },
  },



  plugins: {

 


    blog: {
      autoExcerpt: true,
    },

    // 评论插件配置
    comment: {


      /*
      <script src="https://giscus.app/client.js"
              data-repo="qq547176052/qq547176052.github.io"
              data-repo-id="R_kgDOHbWgbA"
              data-category="Q&A"
              data-category-id="DIC_kwDOHbWgbM4CPZaq"
              data-mapping="url"
              data-reactions-enabled="0"
              data-emit-metadata="0"
              data-input-position="top"
              data-theme="light"
              data-lang="zh-CN"
              data-loading="lazy"
              crossorigin="anonymous"
              async>
      </script>


      */

      /**
       * Using Giscus
       * https://giscus.app/zh-CN
       */
      provider: "Giscus",
      repo: "qq547176052/qq547176052.github.io",
      repoId: "R_kgDOHbWgbA",
      category: "Q&A",
      categoryId: "DIC_kwDOHbWgbM4CPZaq",

      /**
       * Using twikoo
       */
      // type: "twikoo",
      // envId: "https://twikoo.ccknbc.vercel.app",

      /**
       * Using Waline
       */
      // provider:
      // provider: "Waline",
      // // serverURL: "https://vuepress-theme-hope-comment.vercel.app",
      // serverURL: "https://qq547176052-github-bfa5ofvrm-qq547176052.vercel.app",
    },

    mdEnhance: {
      enableAll: true,
      presentation: {
        plugins: ["highlight", "math", "search", "notes", "zoom"],
      },
    },
  },
});
