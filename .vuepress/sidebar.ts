import { sidebar } from "vuepress-theme-hope";
//根据目录生成项目左侧目录---------------------------
const fs = require('fs');
const path = require('path');
function flatten(lists) {
  return lists.reduce((a, b) => a.concat(b), []);
}
function getDirectories(srcpath) {
  return fs.readdirSync(srcpath)
    .map((file) => path.join(srcpath, file))
    .filter(path => fs.statSync(path).isDirectory());
}
function getDirectoriesRecursive(srcpath) {
	return [srcpath, ...flatten(getDirectories(srcpath).map(getDirectoriesRecursive))];
}
const mlarr = getDirectoriesRecursive("./src")
var docml = {}
for (let i = 0; i < mlarr.length; i++) {
	const ml = mlarr[i];
	if(ml.indexOf(".vuepress") == -1 && ml != './src' && ml.lastIndexOf("img") != ml.length-3){
		var m = ml.replace(/src/, "")
		m = m.replace(/\\/g, '/') + "/"
		docml[m] ="structure"
	}
}
// console.log(docml);
//-----------------------------------
//文档左边导航
export default sidebar(
	docml
	// {
	// 	"/doc/": "structure",
	// 	"/项目文档/项目1/":"structure",
	// 	"/项目文档/项目2/":"structure",
	// }
);
