import { navbar } from "vuepress-theme-hope";

//根据目录生成顶部导航---------------------------
const fs = require('fs');
const path = require('path');
//二级目录
function addchildren(mul){
	
	const srcpath = mul.jpath;
	var array = [ ...guolv(srcpath).map((obj)=>{
		var dbml = { 
			text: obj.file, //顶部导航名称
			icon: "edit", //图标
			// prefix: "/" + obj.file,
			link: "/" + obj.file+"/" ,//路径
		}
		return dbml
	})];
	return array
}
//过滤
function guolv(srcpath) {
	return fs.readdirSync(srcpath)
		.map(
			(file) => {
				var jpath = path.join(srcpath, file) 
				return {jpath,file}
			}
		)
		.filter(
			(obj) => {
				var path = obj.jpath

				return fs.statSync(path).isDirectory() &&
					path.indexOf(".vuepress") == -1 &&
					path.lastIndexOf("img") != path.length - 3
			}
		);
}

function getdocml(srcpath) {
	var mul = [ ...guolv(srcpath).map((obj)=>{
		var dbml = { 
			text: obj.file, //顶部导航名称
			icon: "creative", //图标
			prefix: "/" + obj.file,
			link: "/" + obj.file,//路径
		}
		var jpath = obj.jpath
		return {dbml,jpath}
	})];
	for (let index = 0; index < mul.length; index++) {
		mul[index].dbml.children = addchildren(mul[index])
		delete mul[index].jpath
	}
	for (let index = 0; index < mul.length; index++) {
		mul[index] = mul[index].dbml
		if (mul[index].children.length < 1) {
			delete mul[index].children
		}else{
			delete mul[index].link
		}
	}
	return mul
}

function getdaoh(srcpath) {
	var m = getdocml(srcpath)
	return m
}

var daoh = new Array()
var dh = {}
daoh = getdaoh("./src")

dh = { text: "文档分类", icon: "note", link: "/category/" }
daoh.push(dh)
dh = { text: "时间轴", icon: "note", link: "/timeline/" }
daoh.push(dh)

//-----------------------------------


export default navbar(daoh)

// export default navbar(
// 	[
// 		{ text: 'doc', icon: 'creative', prefix: '/doc', link: '/doc' },
// 		{
// 		  text: '1项目文档',
// 		  icon: 'creative',
// 		  prefix: '/项目文档',
// 		  children: [ { text: "项目1", icon: "edit", link: "/" }, { text: "项目2", icon: "edit", link: "/" }]
// 		},
// 		{ text: '随手记', icon: 'creative', link: '/doc/' },
// 		{ text: '文档分类', icon: 'note', link: '/category/' },
// 		{ text: '时间轴', icon: 'note', link: '/timeline/' }
// 	  ]
// )

//顶部导航
/*
export default navbar([
  
  { text: "随手记", icon: "creative", link: "/doc/" },

  {
	text: "项目文档",
	icon: "edit",
	prefix: "/项目文档",
	children: [
	  {
		text: "",
		icon: "edit",
		prefix: "/项目1",
		children: [
		  { text: "项目1", icon: "edit", link: "/" },
		  // { text: "文章 2", icon: "edit", link: "article2" },
		],
	  },


	],
  },

  {
	text: "文档分类",
	icon: "note",
	link: "/category/",
  },

  {
	text: "时间轴",
	icon: "note",
	link: "/timeline/",
  },
]);
*/