---
# 这是侧边栏索引
order: 3
# 这是页面的图标
icon: page
# 这是文章的标题
title: vercel部署前端静态项目
# 设置作者
author: 野生程序员
# 设置写作时间
date: 2022-06-06
# 一个页面可以有多个分类
category:
  - 使用指南
# 一个页面可以有多个标签
tag:
  - vercel
  - 使用指南
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在首页的文章板块中
star: true
# 你可以自定义页脚
footer: qq547176052
comment: true
---

`more` 注释之前的内容被视为文章摘要。

<!-- more -->

## 地址
https://vercel.com/dashboard

## gitee 触发自动构建
![](/img/2022_06_08_00_22_48.png)



![](/img/2022_06_08_00_24_04.png)





