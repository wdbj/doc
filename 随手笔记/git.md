---
# 这是侧边栏索引
order: 1
# 这是页面的图标
icon: page
# 这是文章的标题
title: git命令
# 设置作者
author: 野生程序员
# 设置写作时间
date: 2020-06-06
# 一个页面可以有多个分类
category:
  - git
# 一个页面可以有多个标签
tag:
  - git

# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在首页的文章板块中
star: true
# 你可以自定义页脚
# footer: 这是测试显示的页脚222222
# comment: true
---

`more` git命令

<!-- more -->


## 代理
```
#使用http代理 
git config --global http.proxy http://127.0.0.1:1080
git config --global https.proxy https://127.0.0.1:1080
#使用socks5代理
git config --global http.proxy socks5://127.0.0.1:1080
git config --global https.proxy socks5://127.0.0.1:1080


#使用socks5代理（推荐）
git config --global http.https://github.com.proxy socks5://127.0.0.1:1080
#使用http代理（不推荐）
git config --global http.https://github.com.proxy http://127.0.0.1:1080
```

## 取消代理
```
git config --global --unset http.proxy
git config --global --unset https.proxy
```

## 克隆项目
```
git clone https://github.com/qq547176052/qq547176052.git
```


## 在命令行上创建一个新的仓库
```
echo "# 547176052" >> README.md
git init
git add README.md
git commit -m "1.0.0"

git remote add origin https://github.com/qq547176052/qq547176052.git
git branch -M master
设置分支
git push --set-upstream origin master
git push -u origin master
```

## 推送现有的仓库
```
git add .
git commit -m "1.0.1"
git push -u origin master
```

## 强制推送
```
git push -f origin master
```

## 查看当前仓库地址
```
git remote show origin

```


## 设置新的仓库地址
```
git remote set-url origin https://github.com/qq547176052/547176052.git

git remote add origin https://github.com/qq547176052/547176052.git
```


## 本地项目恢复到指定版本
```
git reset --hard b2232b89cbd104f13a2fdd601fa58931ddeab1fd
```