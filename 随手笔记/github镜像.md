---
# 这是侧边栏索引
order: 10
# 这是页面的图标
icon: page
# 这是文章的标题
title: github镜像
# 设置作者
author: 野生程序员
# 设置写作时间
date: 2022-06-14
# 一个页面可以有多个分类
category:
  - vscode插件
# 一个页面可以有多个标签
tag:
  - github镜像
  - github
# 此页面会在文章列表置顶
sticky: true
# 此页面会出现在首页的文章板块中
star: true
# 你可以自定义页脚
# footer: qq547176052
comment: true
---

`more` 唯一缺点 粘贴快捷键不是 ctrl + v

<!-- more -->



## 镜像网站
https://ghproxy.com/